var DATE_FORMAT = 'DD.MM.YYYY';
var React = require('react');
var ReactDOM = require('react-dom');
var NewLoanBlock = require('./modules/NewLoanBlock');
var LoansTable = require('./modules/LoansTable');
var Datetime = require('react-datetime');
var tomorrow = Datetime.moment().add(1, 'days').format(DATE_FORMAT);
// var Modal = require('react-awesome-modal');
var validator = require('validator');
var $ = require('jquery');

//component goes with capital letter
var App = React.createClass({

   getInitialState: function() {
       return {
           errors: {
              amount: false,
               name: false,
               phone: false,
               iban: false
           },
           validationLength: {
            'name': {
              'min': 2,
              'max': 40
            },
            'phone': {
              'min': 5,
              'max': 20
            }
          },
           // modalVisible: true,
           amount: '',
           date: tomorrow,
           name: null,
           phone: null,
           iban: null,
           existingLoans: this.props.existingLoans
       };
   },


   handleClientInput: function(newState) {
       this.setState(newState);
   },

   handleSubmit: function() {
      var error = false,
        errors = {
          'amount': false,
          'name': false,
          'phone': false,
          'iban': false
        },
        vl = this.state.validationLength;
      /*
       * Date can be inputed only via calendar component, so also no need for validation.
       * There is sense in making additional validation of date in php for better security
       * but this wasn't made in the course of this project.
       */

         errors.amount = !validator.isLength(this.state.amount, 1);
         errors.name = !validator.isLength(this.state.name, vl.name.min, vl.name.max);
         errors.phone = !validator.isLength(this.state.phone, vl.phone.min, vl.phone.max);
         errors.iban = !validator.isAlphanumeric(this.state.iban);

         error = errors.amount || errors.name || errors.phone || errors.iban;
     /* Permitted IBAN characters are the digits 0 to 9 and the 26 upper-case Latin alphabetic characters A to Z.
      * from: http://www.iso.org/iso/catalogue_detail.htm?csnumber=31531
      */

       this.setState({
           errors: errors
       });


      if (!error) {
          var t = this;

          $.post('/store', {
            'amount': this.state.amount,
            'end_date': this.state.date,
            'name': this.state.name,
            'phone': this.state.phone,
            'iban': this.state.iban
          }, function(data) {
              t.handleResponse(data);
          });

       }
   },

   handleResponse: function(data) {

            //if ok - set state
            // else show modal
          // this.setState({
          //       modalVisible : true
          //   });

           this.setState({
               existingLoans: this.state.existingLoans.concat(
                   [{id: data.id, amount: this.state.amount, end_date: this.state.date, name: this.state.name, phone: this.state.phone, iban: this.state.iban}]
               ),
               amount: '',
               date: tomorrow,
               name: null,
               phone: null,
               iban: null
             });
   },

   // closeModal: function() {
   //      this.setState({
   //          modalVisible : false
   //      });
   // },


                // <Modal
                //     visible={this.state.modalVisible}
                //     width="600"
                //     height="180"
                //     effect="fadeInUp">
                //     <div>
                //       lallala
                //         <a href="javascript:void(0);" onClick={this.closeModal}>Close</a>
                //     </div>
                // </Modal>


   render: function() {
       return (
           <div className="app">

               <NewLoanBlock onClientInput={this.handleClientInput} onLoanSubmit={this.handleSubmit} {...this.state} />
               <LoansTable {...this.state} />
           </div>
       );
   }
});

ReactDOM.render(
    <App existingLoans={existingLoans} />,
    document.getElementById('app')
);