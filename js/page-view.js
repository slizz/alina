var React = require('react'),
  ReactDOM = require('react-dom'),
  Datetime = require('react-datetime'),
  LoanInfo = require('./modules/LoanInfo')
  HistoryInfo = require('./modules/HistoryInfo'),

    $ = require('jquery');

//component goes with capital letter
var ViewApp = React.createClass({

   getInitialState: function() {
       return {
           loan: this.props.loan,
           historyRecords: this.props.loan.history_records
       };
   },

  addHistoryRecord: function() {
      var t = this,
          previousRecord = this.state.historyRecords[this.state.historyRecords.length - 1],
          next = Datetime
            .moment(previousRecord.end_date, 'DD.MM.YYYY')
            .add(1, 'weeks')
            .format('DD.MM.YYYY');


      $.get(url + '/extend/', {
        'loan_id': this.props.loan.id,
        'end_date': next
        }, function(data) {

            t.setState({
                historyRecords: t.state.historyRecords.concat([{
                    id: data.id,
                    end_date: next,
                    amount: t.props.loan.amount,
                    start_date: previousRecord.start_date,
                    status: 'Waiting'
                }])
            });


      }.bind(t));

  },

   render: function() {
       return (
           <div>
              <LoanInfo loan={this.props.loan}/>
              <HistoryInfo onAddHistoryRecord={this.addHistoryRecord} historyRecords={this.state.historyRecords}/>
           </div>
       );
   }
});

ReactDOM.render(
   <ViewApp loan={loan} />,
   document.getElementById('app')
);