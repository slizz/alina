var React = require('react');

var ExistingLoanRow = React.createClass({
    render: function() {
        return (
            <a href={url + '/view/' + this.props.existingLoan.id} className="list-group-item">{this.props.existingLoan.amount}EUR until {this.props.existingLoan.end_date}</a>
        );
    }
});

module.exports = ExistingLoanRow;