var React = require('react');
var ExistingLoanRow = require('./ExistingLoanRow');

var LoansTable = React.createClass({
    render: function() {
        var loans = [];

        if (this.props.existingLoans.length > 0) {
            this.props.existingLoans.forEach(function(existingLoan, i){
                loans.push(<ExistingLoanRow existingLoan={existingLoan} key={i + existingLoan.amount + existingLoan.start_date} />);
            });

            return (
                <div className="LoansTable">
                    <div className="list-group">{loans}</div>
                </div>
            );
        } else {
            return false;
        }
    }
});

module.exports = LoansTable;