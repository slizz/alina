var React = require('react'),
    Datetime = require('react-datetime'),
    $ = require('jquery');

var CURRENCY = '€',
    PERCENT_PER_DAY = 0.5,
    baseUrl = url;

var HistoryRecord = React.createClass({
    getInitialState: function() {
        return {
            status: this.props.historyRecord.status
        };
    },

    calculateReturnAmount: function(extensionTime) {
        var startDate = Datetime.moment(this.props.historyRecord.start_date, 'DD.MM.YYYY'),
            initialEndDate = Datetime.moment(this.props.initialEndDate, 'DD.MM.YYYY'),
            diffInDays = Math.ceil(initialEndDate.diff(startDate, 'hours') / 24),
            percentageFromInitialAmount = this.props.historyRecord.amount * diffInDays * PERCENT_PER_DAY / 100;

        return extensionTime === 0
            ? parseFloat((percentageFromInitialAmount + this.props.historyRecord.amount).toFixed(2))
            : parseFloat((((percentageFromInitialAmount * 1.5^(extensionTime - 1))) + this.calculateReturnAmount(extensionTime - 1)).toFixed(2));
    },

    componentDidMount: function() {
        var url = baseUrl + '/status/' + this.props.historyRecord.id,
            t = this,
            interval = setInterval(function() {
                $.get(url, function(data) {
                  if (t.isMounted()) {
                    if (data.status === 'Approved' || data.status === 'Rejected') {
                        t.setState({
                          status: data.status
                        });
                        clearInterval(interval);
                    }
                  }
                }.bind(t));
            }, 5000);
    },

    render: function() {
        var amount = this.calculateReturnAmount(this.props.index);

        return (
            <li className="list-group-item">
                Until {this.props.historyRecord.end_date} needs to return {amount + CURRENCY}.
                Status: {this.state.status}
            </li>
        );
    }
});

module.exports = HistoryRecord;