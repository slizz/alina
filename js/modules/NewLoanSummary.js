var React = require('react');
var CURRENCY = '€';
var PERCENT_PER_DAY = 0.5;
var Datetime = require('react-datetime');

var NewLoanSummary = React.createClass({

    calculateReturnAmount: function() {
        var today = Datetime.moment(),
            loadLastDay = Datetime.moment(this.props.date, 'DD.MM.YYYY'),
            diffInDays = Math.ceil(loadLastDay.diff(today, 'hours') / 24);

            //had to make diff in hours first, not in days, bc 23 hours are being count as 0 days, even though on calendar it's 1 day

        // if (extensionTime === 0) {
        //     return (parseFloat(this.props.amount, 2) * (weeks * 3) / 100) + parseFloat(this.props.amount, 2);
        // } else {
        //     return ((parseFloat(this.props.amount, 2) * (weeks * 3) * 1.5^(extensionTime - 1)) / 100)
        //             + this.calculateReturnAmount(weeks, extensionTime - 1);
        // }
        return (parseFloat(this.props.amount, 2) * (diffInDays * PERCENT_PER_DAY) / 100) + parseFloat(this.props.amount, 2);

    },

    render: function() {
        var returnAmount = this.calculateReturnAmount();
        var returnBlockClass = (this.props.amount === '' || this.props.date === '') ? '_hide' : '_show';
        return (
            <div className="NewLoanSummary">
                <h2>Take: {this.props.amount}{CURRENCY}</h2>
                <h2>Until: {this.props.date}</h2>
                <h2 className={returnBlockClass}>Return: {returnAmount}{CURRENCY}</h2>
            </div>
        );
    }
});

module.exports = NewLoanSummary;