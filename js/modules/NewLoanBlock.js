var React = require('react');
var NewLoanForm = require('./NewLoanForm');
var NewLoanSummary = require('./NewLoanSummary');

var NewLoanBlock = React.createClass({
    render: function() {
        return (
            <div className="NewLoanBlock">
                <h1>Take a loan (max - 500 eur for 2 months)</h1>
                <div>
                    <NewLoanForm {...this.props} />
                    <NewLoanSummary {...this.props} />
                </div>
            </div>
        );
    }
});

module.exports = NewLoanBlock;