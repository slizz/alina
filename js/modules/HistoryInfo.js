var React = require('react'),
    Datetime = require('react-datetime'),
    HistoryRecord = require('./HistoryRecord');

var LOAN_STATUS_APPROVED = 'Approved';

var NewLoanSummary = React.createClass({
    setInitialState: function() {
        historyRecords: this.props.historyRecords
    },

    onAddHistoryRecord: function() {
        if (confirm('Are you sure?')) {
            this.props.onAddHistoryRecord();
        }
    },

    lastLoanRecordIsApproved: function() {
        return this.status.historyRecords[this.status.historyRecords.length - 1].status !== LOAN_STATUS_APPROVED;
    },

    render: function() {
        var historyRecords = [],
            initialEndDate = this.status.historyRecords[0].end_date;

        this.status.historyRecords.forEach(function(historyRecord, i){
            historyRecords.push(<HistoryRecord initialEndDate={initialEndDate} historyRecord={historyRecord} index={i} key={i + historyRecord.id} />);
        });

        return (
            <div className="historyInfo">
                <h1>Loan history</h1>
                <button disabled={this.lastLoanRecordIsApproved()} onClick={this.onAddHistoryRecord} className="btn btn-default extend">Extend</button>
                <div className="historyList">{historyRecords}</div>
            </div>
        );
    }
});

module.exports = NewLoanSummary;