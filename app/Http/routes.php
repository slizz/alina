<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/test', function(){


});

//Main
$app->get('/',[
	'as' => 'loan.index', 'uses' => 'LoanController@index'
]);

//View information
$app->get('/view/{id}', [
    'as' => 'loan.view', 'uses' => 'LoanController@view'
]);

//History record
$app->get('/status/{id}', [
    'as' => 'loan.status', 'uses' => 'LoanController@getHistoryStatus'
]);

//Extend loan
$app->get('/extend', [
    'as' => 'loan.extend', 'uses' => 'LoanController@extend'
]);

//Store information
$app->post('/store', [
	'as' => 'loan.store', 'uses' => 'LoanController@store'
]);