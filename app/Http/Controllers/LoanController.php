<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Loan;
use App\Models\LoanHistory;
use Validator;
use Carbon\Carbon;

class LoanController extends BaseController
{

    /**
     * Display home page
     * @return View
     */
    public function index()
    {
        $loans = Loan::get();
        $loans->each(function($loan){
            $loan->end_date = $loan->historyRecords()->get()->last()->end_date;
        });

        return view('index', [
            'loans' => $loans->toJson()
        ]);
    }

    /**
     * Check status of history record
     * @param int $hrId id of history record
     * @return Response
     */
    public function getHistoryStatus($hrId)
    {
        $lh = LoanHistory::findOrFail($hrId);
        $createdAtTime = strtotime($lh->created_at);
        $currentTime = time();

        if($currentTime - $createdAtTime  > 30) {
            $lh->status = (rand(0,100)) > 30 ? "Approved" : "Rejected"; //70% of approved and 30% of rejected
            $lh->save();
        }

        return response()->json(['status'=>$lh->status]);
    }

    /**
     * Extend current loan
     * @return Response
     */
    public function extend(Request $request)
    {
        $loan = Loan::findOrFail($request->get('loan_id'));

        $historyRecord = $loan->historyRecords()->create([
                'end_date' => $request->get('end_date'),
                'status' => 'Waiting',
                'created_at' => date('Y-m-d H:i:s')
            ]);

        return response()->json([
                'id' => $historyRecord->id
            ]);
    }

    /**
     * Store loan information to database
     * @return Response
     */
    public function store(Request $request)
    {
        // Validation rules
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
            'end_date' => 'required',
            'name' => 'required',
            'iban' => 'required',
            'phone' => 'required',
        ]);

        // If validation fails, provide errors
        if ($validator->fails()) {
            return response()->json([
                'success' => 0,
                'messages' => $validator->errors()->all()
            ]);
        }

        // Risk assessment
        // 1. 3 loans from single ip per 24 hours
        // 2. Max amout between 00:00 and 06:00
        
        $loans = Loan::where('ip', '=', $request->ip()) 
            ->where('end_date', '>=', Carbon::now()->subDay())->get();

        $loanLimitPerDay = ($loans->count() >= 3);
        $amountOnTime = ($request->get('amount') >= 500); //Time between is missing

        if ($loanLimitPerDay || $amountOnTime) {
            return response()->json([
                'sorry'
            );
        }



        // Loan record
        $loan = Loan::create([
            'amount' => $request->get('amount'),
            'start_date' => date('Y-m-d'),
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'iban' => $request->get('iban'),
            'ip' => $request->ip()
        ]);

        // Loan history record
        $loanHistory = $loan->historyRecords()->create([
            'end_date' => $request->get('end_date'),
            'status'=> 'Waiting', // initial status
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return response()->json([
            'id' => $loan->id
        ]);
    }
    /**
     * View loan intormation and extensions for a certain loan.
     *
     * @param  int  $id
     * @return Response
     */
    public function view($id)
    {
        $loan = Loan::with('historyRecords')->findOrFail($id);
        $loan->historyRecords->each(function ($history) use ($loan) {
            $history->start_date = $loan->start_date;
            $history->amount = $loan->amount;
        });

        // dd($loan->toArray());
        return view('view', [
                'loan' => $loan->toJson()
            ]);
    }
}
