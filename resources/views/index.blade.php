<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="/css/main.css"/>
      <meta charset="UTF-8">
  </head>
  <body>

    <div id="app"></div>

    <script>
        var url = "{{app('url')->to('/')}}";
        var existingLoans = {!! $loans !!};
    </script>
    <script src="/js/vendors.js"></script>
    <script src="/js/page-main.bundle.js"></script>
  </body>
</html>